<?php
    include_once('connection.php');
    $id = 0;
    $data = [];
     
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
        $data = $collection->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
    }
     
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['id'];
         
        $collection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
        header("Location: index.php");
         
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</head>
 
<body>
    <div class="container">
        <div class="span10 offset1">
            <div class="row">
                <h3>Delete a Customer</h3>
            </div>
            <form class="form-horizontal" action="delete.php" method="post">
                <input type="hidden" name="id" value="<?php echo $id;?>"/>
                <p class="alert alert-error">Are you sure to delete <?php echo $data['name'];?>?</p>
                <div class="form-actions">
                    <button type="submit" class="btn btn-danger">Yes</button>
                    <a class="btn btn-info" href="index.php">No</a>
                </div>
            </form>
        </div>
    </div> <!-- /container -->
  </body>
</html>