<?php
require 'vendor/autoload.php'; // include Composer's autoloader

$dbHost = 'localhost';
$port = '27017';
$dbName = 'crud';
$collectionName = 'contacts';

$client = new MongoDB\Client("mongodb://$dbHost:$port");
$collection = $client->$dbName->$collectionName;
?>
