<?php
$data = [];
    include_once('connection.php');
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: index.php");
    } else {
        $data = $collection->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</head>
 
<body>
    <div class="container">
        <div class="span10 offset1">
            <div class="row">
                <h3>Read a Customer</h3>
            </div>
            <div class="form-horizontal" >
                <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                    <label class="checkbox">
                        <?php echo $data['name'];?>
                    </label>
                </div>
                </div>
                <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                    <label class="checkbox">
                        <?php echo $data['address'];?>
                    </label>
                </div>
                </div>
                <div class="form-actions">
                    <a class="btn btn-info" href="index.php">Back</a>
                </div>
            </div>
        </div>
    </div> <!-- /container -->
  </body>
</html>