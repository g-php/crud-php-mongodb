
<?php
    include_once('connection.php');
    if ( !empty($_POST)) {
        // keep track validation errors
        $nameError = null;
        $addressError = null;

        // keep track post values
        $name = $_POST['name'];
        $address = $_POST['address'];
         
        // validate input
        $valid = true;
        if (empty($name)) {
            $nameError = 'Please enter Name';
            $valid = false;
        }
         
        if (empty($address)) {
            $addressError = 'Please enter Address';
            $valid = false;
        }
         
        // insert data
        if ($valid) {
            $collection->insertOne(['name' => $name, 'address' => $address]);
            header("Location: index.php");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</head>
 
<body>
    <div class="container">
        <div class="span10 offset1">
            <div class="row">
                <h3>Create a Customer</h3>
            </div>
            <form class="form-horizontal" action="create.php" method="post">
                <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                    <label class="control-label">Name</label>
                    <div class="controls">
                        <input class="form-control <?php echo empty($nameError)?'':'is-invalid';?>" name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
                        <?php if (!empty($nameError)): ?>
                        <span class="<?php echo empty($nameError)?'':'invalid-feedback';?>"><?php echo $nameError;?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <br>
                <div class="control-group <?php echo !empty($addressError)?'error':'';?>">
                    <label class="control-label">Address</label>
                    <div class="controls">
                        <input class="form-control <?php echo empty($addressError)?'':'is-invalid';?>" name="address" type="text" placeholder="Address" value="<?php echo !empty($address)?$address:'';?>">
                        <?php if (!empty($addressError)): ?>
                            <div class="<?php echo empty($addressError)?'':'invalid-feedback';?>"><?php echo $addressError;?></div>
                        <?php endif;?>
                    </div>
                </div>
                <br>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Create</button>
                    <a class="btn btn-info" href="index.php">Back</a>
                </div>
            </form>
        </div>
    </div> <!-- /container -->
</body>
</html>    